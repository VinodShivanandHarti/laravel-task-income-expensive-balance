<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
	protected $table= "income_expense";

    protected $fillable = [
        'type', 'amount', 'details', 'date',
    ];

}
