<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Form;
use DB;
class formController extends Controller
{

  public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function insert(Request $request){

		$this->validate($request, [
			'type' => 'required',
			"amount" => "required",
			"details" => "required",
			"date" => "date"
		]);

		$model = new Form();

		if($request->input('type')) $model->type = $request->input('type');
		if($request->input('amount')) $model->amount = $request->input('amount');
		if($request->input('details')) $model->details = $request->input('details');
		if($request->input('date')) $model->date = $request->input('date');
		
		$model->save();

		return redirect()->back()->with('success', 'Inserted Successfully');           
	}

	public function show_reports(){
		$model = Form::get();
		$sum = Form::where('type', '=', 0)->sum('amount');
		$dif = Form::where('type', '=', 1)->sum('amount');
		$total = $sum-$dif;
		
		return view('show_reports', compact('model','total'));
	}

	public function update($id){
        $data = Form::where('id', '=', $id)->get();
        
        return view('update', compact('data'));
    }

    public function edit_update(Request $request, $id)
    {
        DB::table('income_expense')
        ->where('id', $id)
        ->update(['type' => $request->type,
        	'amount' => $request->amount,
        	'details' => $request->details,
        	'date' => $request->date]);

        $data = Form::all();

		return redirect()->back()->with('success', 'Updated Successfully');
    }

    public function delete(Request $request, $id){
		$a = DB::table('income_expense')->where('id', $id)->delete();

       return redirect()->back()->with('delete', 'Deleted Successfully');
    }

}
