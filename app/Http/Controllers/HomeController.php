<?php
   
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use App\Form;
   
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
  
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
  
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function adminHome()
    {
            $sum = Form::where('type', '=', 0)->sum('amount');
        $dif = Form::where('type', '=', 1)->sum('amount');
        $total = $sum-$dif;
        return view('adminHome', compact('total'));
    }
    
}