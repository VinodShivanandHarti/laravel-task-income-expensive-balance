@extends('layouts.app')

@section('content')

 @if(session()->has('delete'))
<div class="alert alert-danger">
    {{ session()->get('delete') }}
</div>
@endif

<div class="container">

<button class="btn btn-warning" onclick="return confirm('Total Balance : {{$total}}')">Balance</button>

<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Type (Income / Expense)</th>
      <th scope="col">Amount</th>
      <th scope="col">Details</th>
      <th scope="col">Date</th>
      <th scope="col">Update</th>
      <th scope="col">Delete</th>
    </tr>

  </thead>
  <tbody>

    @foreach($model as $data)
    <tr>
      <td scope="col">{{$data->id}}</td>
      @if($data->type == '0')
      <td scope="col">Income</td>
      @else
      <td scope="col">Expense</td>
      @endif

      @if($data->type == '0')
      <td scope="col">+ {{$data->amount}}</td>
      @else
      <td scope="col">- {{$data->amount}}</td>
      @endif
      <td scope="col">{{$data->details}}</td>
      <td scope="col">{{$data->date}}</td>
      <td><a href="update/{{$data->id}}"><button type="button" class="btn btn-primary">Update</button></a></td>
      <td><a onclick="return confirm('Are you sure? You want to delete the Record')" href="delete/{{$data->id}}"><button type="button" class="btn btn-danger">Delete</button></a></td>
      @endforeach
    </tr>
  </tbody>
<tfoot>
  <tr>
    <th><b>Total</b></th>
    <th></th>
    <th><b>{{$total}}</b></th>
    <th></th>
    <th></th>
    <th></th>
    <th></th>
    
  </tr>
  <tr>
    <th></th>
    <th></th>
    <th></th>
    <th></th>
    <th></th>
    <th></th>
    <th></th>    
    
  </tr>
</tfoot>

</table>
</div>


<script>
function alert() {
  alert ('<?php echo $total; ?>');
}
</script>



@endsection