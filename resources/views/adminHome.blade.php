@extends('layouts.app')

@section('content')

@if(session()->has('success'))
<div class="alert alert-success">
    {{ session()->get('success') }}
</div>
@endif

<div class="container">
 
<button class="btn btn-warning" onclick="return confirm('Total Balance : {{$total}}')">Balance</button>

  <h2>Form Submit</h2>

  <form method="post" action="{{route('post_form')}}">
             {{ csrf_field() }} 

    <div class="form-group">
      <label for="type">Type:</label>
      <select name="type" id="type" class="form-control">
        <option value='0'>Income</option>
        <option value='1'>Expense</option>
    </select>
</div'>

<div class="form-group">
  <label for="amount">Amount:</label>
  <input type="number" class="form-control" id="pwd" placeholder="Enter amount" name="amount" required>
</div>

<div class="form-group">
  <label for="details">Enter Details:</label>
  <textarea class="form-control" id="details" placeholder="Enter details" name="details" required></textarea> 
</div>

<div class="form-group">
  <label for="date">Date:</label>
  <input type="date" class="form-control" id="date" name="date" required>
</div>

<button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>



@endsection