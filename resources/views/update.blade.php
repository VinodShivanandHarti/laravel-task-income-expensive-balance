@extends('layouts.app')

@section('content')

@if(session()->has('success'))
<div class="alert alert-success">
    {{ session()->get('success') }}
</div>
@endif

<div class="container">
 
  <h2>Form Update</h2>

@foreach ($data as $value)
    
 <form name="myForm" action="/admin/edit_update/{{ $value->id }}" method="post">
  
  {{ csrf_field() }} 
    <div class="form-group">
      <label for="type">Type:</label>
      <select name="type" id="type" class="form-control">
        <option value="0" {{ old('type', $value->type) == 0 ? 'selected' : '' }}>Income</option>
        <option value='1' {{ old('type', $value->type) == 1 ? 'selected' : '' }}>Expense</option>
    </select>
</div'>

<div class="form-group">
  <label for="amount">Amount:</label>
  <input type="number" class="form-control" id="pwd" placeholder="Enter amount" name="amount" value="{{$value->amount}}" required>
</div>

<div class="form-group">
  <label for="details">Enter Details:</label>
  <textarea class="form-control" id="details" placeholder="Enter details" name="details" required>{{$value->details}}</textarea> 
</div>

<div class="form-group">
  <label for="date">Date:</label>
  <input type="date" class="form-control" id="date" name="date" value="{{$value->date}}" required>
</div>

@endforeach

<button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>



@endsection