<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('admin/home', 'HomeController@adminHome')->name('admin.home')->middleware('is_admin');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/post_form', 'formController@insert')->name('post_form');
Route::get('/admin/show_reports', 'formController@show_reports')->name('/admin/show_reports');
Route::get('admin/update/{id}', 'formController@update');
Route::post('/admin/edit_update/{id}', 'formController@edit_update');
Route::get('admin/delete/{id}', 'formController@delete');